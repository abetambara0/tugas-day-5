import React from "react";
import Navbar from "./Components/Navbar";
import MainRoutes from "./Routing/MainRoutes";
import { createMuiTheme, createTheme, ThemeProvider } from '@mui/material';

const theme = createMuiTheme({
  typography: {
    fontFamily: [
      'Roboto Slab', 
      'serif',
      'Ubuntu'
    ].join(',')
  },});

function App() {
  return (
    <>
    <ThemeProvider theme={theme}>
    <Navbar/>
    <MainRoutes/>
    </ThemeProvider>
    </>
  );
}

export default App;
