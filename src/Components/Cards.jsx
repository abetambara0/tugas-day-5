import React from "react";
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';



const Cards = ({ title, date, desc, img }) => {


    return (
        <Card sx={{ maxWidth: 325, margin: "10px" }}>
            <CardHeader
                avatar={
                    <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                        R
                    </Avatar>
                }
                title={title}
                subheader={date}
            />
            <CardMedia
                component="img"
                height="194"
                image={img}
                alt="Paella dish"
            />
            <CardContent>
                <Typography sx={{ fontFamily: 'Ubuntu' }} variant="body2" color="text.secondary">
                    {desc}
                </Typography>
            </CardContent>
        </Card>
    );
};


export default Cards;
